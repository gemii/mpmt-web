/**
 * Created by jiayi.hu on 1/24/17.
 */
var contrail = {
    run: function () {
        // contrail.directionJudge();
        this.pullUpLoading();
        this.pullDownLoading();

    },
    pullUpLoading: function () {
//             var container = document.querySelector('.container');
//             container.addEventListener('scroll', function () {
//                 if (is_touch) return;
//                 var scroll_height = container.scrollHeight;
//                 var scroll_top = container.scrollTop;
//                 var offset_height = container.offsetHeight;
//                 console.log(scroll_height, scroll_top, offset_height);
//                 if (!is_touch && scroll_top >= scroll_height - offset_height) {
//
//                 } else {
//                     return;
//                 }
//             });     //浏览过程中与下拉不形成干扰
//             var is_touch = false;
//             var touchDown = null;
//             var scroll_top;
//             container.addEventListener('touchstart', function () {
//                 is_touch = true;
//             }, false);
//             container.addEventListener('touchmove', function (event) {
//                 var scroll_height = container.scrollHeight;
//                 scroll_top = container.scrollTop;
//                 var offset_height = container.offsetHeight;
//                 console.log(scroll_height, scroll_top, offset_height);
//                 if (scroll_top <= 0) {
//                     console.log("1");
//                     var touchY = event.touches[0].pageY;
//                     touchDown = touchY - 100 ;
//                     if(touchDown >= 120){
//                         touchDown = 120;
//                     }
//                     container.style.transform = "translate3D(0px," + touchDown + "px,0px)";
//                     // $(".upLoading").css({"display":"block"});
//                     $(".upLoading").css({"display":"block"});
//                     // $(".loading").css({"display":"block"});
//
//                 }else {
//                     container.style.transform = "translate3D(0px,0px,0px)";
//                 }
//                 // container.style.transtion = "all 1s";
//             })
//             container.addEventListener('touchend', function () {
//                 is_touch = false;
//                 console.log(is_touch);
//                 if(touchDown >= 0&&scroll_top <= 0){
//                     var upLoading = document.getElementsByClassName("upLoading");
//                     container.style.transform = "translate3D(0px,0px,0px)";
//                     container.style.transition = "all 1s";
//                     setTimeout(function () {
//                         var dataNext = dataModel.getData("json/package.json");
//                         if(!(dataNext == undefined || dataNext == "")){
//                             $(".upLoading").slideUp();
//                             console.log(dataNext);
//                             $.each(dataNext.exclude, function (k, v) {
//                                 $(".detail_ul").prepend(
//                                     "<li class='detail_li'>" +
//                                     "<div class='taskMark'></div>" +
//                                     "<div class='taskIcon'></div>" +
//                                     "<div class='taskName'>" +
//                                     "<b class='name'>" +"&nbsp;&nbsp;&nbsp;"+ v.name + "</b><br>" +
//                                     "<a class='date'>时间: <b>" + v.date + "</b></a>" +
// //                    "<a class='integral'>积分+<b>" + v.integral + "</b></a>" +
//                                     "</div>" +
//                                     "<div class='taskStatus'>" +
//                                     "<a class='status'>" + v.status + "&nbsp;+" + v.integral + "</a>" +
//                                     "</div>" +
//                                     "</li>"
//                                 );
//                             })
//                         }
//
//                     },2000);
//
//                     // $(".upLoading").slideUp();
//                 }else {
//                     // container.style.transform = "translate3D(0px,-100px,0px)";
//                     container.style.transform = "translate3D(0px,0px,0px)";
//                     $(".upLoading").slideUp();
//                 }
//             }, false);
    },
    pullDownLoading: function () {
        var container = document.querySelector('.container');
        container.addEventListener('scroll', function () {
            if (is_touch) return;
            var scroll_height = container.scrollHeight;
            var scroll_top = container.scrollTop;
            var offset_height = container.offsetHeight;
            if (!is_touch && scroll_top >= scroll_height - offset_height) {

            } else {
                return;
            }
        })            //浏览过程中与上拉不形成干扰
        var is_touch = false;
        var touchDown = null;
        var pageNo = 1;
        var scroll_top, scroll_height, offset_height;
        var startY;
        container.addEventListener('touchstart', function (event) {
            startY = event.touches[0].pageY;
            is_touch = true;
        }, false);
        container.addEventListener('touchmove', function (event) {
            scroll_height = container.scrollHeight;
            scroll_top = container.scrollTop;
            offset_height = container.offsetHeight;
            if (scroll_top == scroll_height - offset_height) {
                var touchY = event.touches[0].pageY;
                if (touchY - startY > 0) {
                    return;
                }
                touchDown = touchY - offset_height;
                if (touchDown <= -120) {
                    touchDown = -120;
                }
                container.style.transform = "translate3D(0px," + touchDown + "px,0px)";
                $(".downLoading").css({"display": "block"});

            } else {
                container.style.transform = "translate3D(0px,0px,0px)";
                // $(".downLoading").css({"display": "none"});
            }
        })
        var flag = false;
        container.addEventListener('touchend', function () {
            is_touch = false;
            var total, pageSize;
            if (touchDown <= 0 && scroll_top == scroll_height - offset_height) {
                container.style.transform = "translate3D(0px,0px,0px)";
                container.style.transition = "all 1s";
                setTimeout(function () {
                    pageNo += 1;
                    if (flag == true) {
                        // $(".downLoading").slideUp();
                        $(".downLoading_img").css({"display": "none"});
                        $(".downLoading").text("- 没有更多了 -");
                        setTimeout(function () {
                            $(".downLoading").slideUp();
                        }, 1000)

                        return;
                    }
                    if (htmlJudge == true) {
                        var dataPre = dataModel.getListInterationRecord(pageNo);
                        total = dataPre.page.total;
                        pageSize = dataPre.page.pageSize;
                        if (!(dataPre == undefined || dataPre == "")) {
                            if (!(total == undefined || total == "")) {
                                if (pageNo + 1 > Math.round(total / pageSize)) {
                                    flag = true;
                                    $(".downLoading_img").css({"display": "none"});
                                    $(".downLoading").text("- 没有更多了 -");
                                    setTimeout(function () {
                                        $(".downLoading").slideUp();
                                    }, 1000)
                                }
                            } else {
                                $(".downLoading").slideUp();

                                $.each(dataPre.page.rows, function (k, v) {
                                    $(".detail_ul").append(
                                        "<li class='detail_li'>" +
                                        "<div class='taskMark' id='taskMark_" + pageNo + "_" + v.type + "'></div>" +
                                        "<div class='taskIcon' id='taskIcon_" + pageNo + "_" + v.type + "_" + v.tasksType + "'></div>" +
                                        "<div class='taskName'>" +
                                        "<b class='name'>" + "&nbsp;&nbsp;&nbsp;" + v.title + "</b><br>" +
                                        "<a class='date'>时间: <b>" + v.createTimeStr + "</b></a>" +
                                        "</div>" +
                                        "<div class='taskStatus taskStatus_"+pageNo+ "_" + v.type + "'>" +
                                        "<a class='status' id='status_" + pageNo + "_" + v.type + "'>" + "</a>" + "<a class='status'>" + "&nbsp;" + v.integration + "</a>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    controller.getTaskMark(v.tasksType, pageNo, v.type);
                                })
                            }
                        }
                    } else {
                        var historyDataPre = dataModel.getListTaskMembers(pageNo);
                        total = historyDataPre.page.total;
                        pageSize = historyDataPre.page.pageSize;
                        if (!(historyDataPre == undefined || historyDataPre == "")) {
                            if (!(total == undefined || total == "")) {
                                if (pageNo + 1 > Math.round(total / pageSize)) {
                                    flag = true;
                                    $(".downLoading_img").css({"display": "none"});
                                    $(".downLoading").text("- 没有更多了 -");
                                    setTimeout(function () {
                                        $(".downLoading").slideUp();
                                    }, 1000)
                                }
                            } else {
                                $(".downLoading").slideUp();

                                $.each(historyDataPre.page.rows, function (k, v) {
                                    $(".detail_ul").append(
                                        "<li class='viewDetail_li'>" +
                                        "<div class='taskIcon' id='taskIcon_" + pageNo + "_" + v.typeId + "'></div>" +
                                        "<div class='taskName'>" +
                                        "<b class='name'>" + v.title + "</b><br>" +
                                        "<a class='date'>时间: <b>" + v.endDateStr + "</b></a>" +
                                        "</div>" +
                                        "<div class='viewTaskStatus'>" +
                                        "<a class='status' id='status_" + pageNo + "_" + v.state + "'>" + v.state + "</a>" +
                                        "</div>" +
                                        "</li>"
                                    );
                                    controller.getHistoryTaskMark(pageNo, v.typeId, v.state);
                                })
                            }
                        }
                    }

                }, 2000);
            } else {
                container.style.transform = "translate3D(0px,0px,0px)";
                $(".downLoading").slideUp();
            }
        }, false);
    }
}
var controller = {
    getTaskMark: function (typeNo, No, id) {
        var taskMark = document.querySelectorAll("#taskMark_" + No + "_" + id);
        var taskIcon = document.querySelectorAll("#taskIcon_" + No + "_" + id + "_" + typeNo);
        var taskStatus = document.querySelectorAll(".taskStatus_"+ No +"_"+ id);
        var status = document.querySelectorAll("#status_" + No + "_" + id);
        switch (id) {
            case "0" :
                $(taskMark).css({"background": "#F7E233"});
                $(taskIcon).css({"background": "#FEE2D1 url('images/checkIn_small.png')no-repeat center center"});
                $(status).text("签到成功");
                break;
            case "1" :
                $(taskMark).css({"background": "#FB673F"});
                $(taskIcon).css({"background": "#FEE2D1 url('images/invite_small.png')no-repeat center center"});
                $(status).text("邀请成功");
                break;
            case "2" :
                $(taskMark).css({"background": "#B1E4FD"});
                $(status).text("审核通过");
                switch (typeNo) {
                    case "0" :
                        $(taskIcon).css({"background": "#FEE2D1 url('images/questionnaire_small.png')no-repeat center center"});
                        break;
                    case "1" :
                        $(taskIcon).css({"background": "#FEE2D1 url('images/report_small.png')no-repeat center center"});
                        break;
                    case "2" :
                        $(taskIcon).css({"background": "#FEE2D1 url('images/tryout_small.png')no-repeat center center"});
                        break;
                }
                break;
            case "3" :
                $(taskMark).css({"background": "#FCAC35"});
                $(taskIcon).css({"background": "#FEE2D1 url('images/exchange.png')no-repeat center center"});
                $(status).text("兑换成功");
                $(taskStatus).css({"color":"#FCAC35"});
                break;
            case "4" :
                $(taskMark).css({"background": "#19EFB2"});
                $(taskIcon).css({"background": "#FEE2D1 url('images/joinUs.png')no-repeat center center"});
                $(status).text("注册成功");
                break;
            case "5":
                $(taskMark).css({"background": "#FCAC35"});
                $(taskIcon).css({"background": "#FEE2D1 url('images/fill_small.png')no-repeat center center"});
                $(status).text("填写资料");
        }
    },
    getHistoryTaskMark: function (No, typeId, state) {
        var taskIcon = document.querySelectorAll("#taskIcon_" + No + "_" + typeId);
        var taskState = document.querySelectorAll("#status_" + No + "_" + state);
        switch (typeId) {
            case "0" :
                $(taskIcon).css({"background": "#FEE2D1 url('images/questionnaire_small.png')no-repeat center center"});
                break;
            case "1" :
                $(taskIcon).css({"background": "#FEE2D1 url('images/report_small.png')no-repeat center center"});
                break;
            case "2" :
                $(taskIcon).css({"background": "#FEE2D1 url('images/tryout_small.png')no-repeat center center"});
                break;

        }
        if (state == "已完成" || state == "已通过") {
            $(taskState).css({"color": "#fd6837"});
        }
    }
}