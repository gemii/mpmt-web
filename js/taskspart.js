/**
 * Created by jiayi.hu on 2/17/17.
 */
var taskIcon, homeTaskIcon, receiveEndTime, taskStatus, homeTaskStatus, homeTaskDetail, taskCardReward, taskReward, receiveBtn;
var unionid = $.cookie("unionid_cookie");
var taskController = {
    swiperReload: function () {
        var swiper = new Swiper('.swiper-container', {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            observer: true,
            observeParents: true,
            coverflow: {
                rotate: 0,
                stretch: -45,
                depth: 100,
                modifier: 1,
                slideShadows: false
            }
        });
    },
    taskCardsReload: function () {
        var tasksInit = dataModel.getPersonTasksInit(unionid);
        $(".swiper-wrapper").empty();
        if (tasksInit.data.length == 0) {
            $(".swiper-wrapper").append('<div class="swiper-slide" style="background: #fff">' +
                '<div class="taskCard_leftBox">' +
                '<div class="leftBox_icon"></div>' +
                '</div>' +
                '<div class="taskCard_rightBox">' +
                '<a class="rightBox_note1">亲爱的妈妈～每周微任务即将上线，快加入栗妈闺蜜团，做任务赢栗子，就有机会兑换丰厚礼品哦!</a><br>' +
                '<a class="rightBox_note2">记得每日签到, 轻松领栗子哟~</a>' +
                '</div>');
        }
        $.each(tasksInit.data, function (k, v) {
            $(".swiper-wrapper").append(
                '<div class="swiper-slide" style="background: #fff">' +
                '<div class="taskCard_leftBox">' +
                // '<div class="taskCard_liziBox" id="taskCard_lizi_'+ v.id+'">' +
                // '<div class="taskCard_lizi" >+' + v.integration + '</div>' +
                // '</div>' +
                '<div class="receiveIcon" id="homeTaskIcon_' + v.typeId + '"></div>' +
                '<div class="taskCard_detailBtn" id="homeTaskDetail_' + v.id + '"><a class="a_detail">' + "任务详情" + '</a></div>' +
                '</div>' +
                '<div class="taskCard_rightBox">' +
                '<p class="receiveTitle">' + v.title + '</p>' +
                '<p class="receiveStaAndEnd">' + v.shelveDateStr + "-" + v.endDateStr + '</p>' +
                '<p class="taskCardReward" id="taskReward_' + v.id + '"></p>' +
                '<p class="taskDetail"><a class="taskIntro">' + controller.null2string(v.taskBrief) + '</a><br><a style="display: none" class="receiveEndTime" id="receiveEndTime_' + v.typeId + '">申领期限:' + v.applyEndDateStr + '</a></p>' +
                '<div class="receiveBtn" id="homeTaskStatus_' + v.id + '"></div></div>' +
                '</div>'
            )
            taskController.btnStatusJudge(v.typeId, v.id, v.integration, v.elecReward, v.entityReward, v.state, v.taskUrl);
        })
    },

    btnStatusJudge: function (typeId, taskId, integration, elecReward, entityReward, state, taskUrl) {
        taskReward = document.querySelectorAll("#taskReward_" + taskId);
        taskCardReward = document.querySelectorAll("#taskCardReward_" + taskId);
        taskIcon = document.querySelectorAll("#taskIcon_" + typeId);
        homeTaskIcon = document.querySelectorAll("#homeTaskIcon_" + typeId);
        receiveEndTime = document.querySelectorAll("#receiveEndTime_" + typeId);
        taskStatus = document.querySelectorAll("#taskStatus_" + taskId);
        homeTaskStatus = document.querySelectorAll("#homeTaskStatus_" + taskId);
        homeTaskDetail = document.querySelectorAll("#homeTaskDetail_" + taskId);
        receiveBtn = document.querySelectorAll(".receiveBtn");
        if (!(integration == undefined || integration == null || integration == "")) {
            if (!(taskReward == 0)) {
                $(taskReward).text("栗子 +" + integration);
                $(taskReward).css({
                    "background": "url('images/lizi_img.png')no-repeat 0% center",
                    "text-indent": "32px"
                });
            }
        } else if (!(elecReward == undefined || elecReward == null || elecReward == "")) {
            if (!(taskReward == 0)) {
                $(taskReward).text("电子券: " + elecReward);
            }
        } else if (!(entityReward == undefined || entityReward == null || entityReward == "")) {
            if (!(taskReward == 0)) {
                $(taskReward).text("奖品: " + entityReward);
            }
        } else {
            if (!(taskReward == 0)) {
                $(taskReward).text("");
            }
        }
        switch (typeId.toString()) {
            case "0":
                if (!(homeTaskIcon == 0)) {
                    $(homeTaskIcon).css({"background": "#FEE2D1 url('images/questionnaire.png')no-repeat center center"});
                }
                if (!(taskIcon == 0)) {
                    $(taskIcon).css({"background": "#FEE2D1 url('images/questionnaire_small.png')no-repeat center center"});
                }
                break;
            case "1":
                if (!(homeTaskIcon == 0)) {
                    $(homeTaskIcon).css({"background": "#FEE2D1 url('images/report.png')no-repeat center center"});
                }
                if (!(taskIcon == 0)) {
                    $(taskIcon).css({"background": "#FEE2D1 url('images/report_small.png')no-repeat center center"});
                }
                break;
            case "2":
                if (!(homeTaskIcon == 0)) {
                    $(homeTaskIcon).css({"background": "#FEE2D1 url('images/try_out.png')no-repeat center center"});
                    $(receiveEndTime).css({"display": "block"});
                }
                if (!(taskIcon == 0)) {
                    $(taskIcon).css({"background": "#FEE2D1 url('images/tryout_small.png')no-repeat center center"});
                }
                break;
        }
        // switch (state) {
        //     case "待完成" :
        if (state == "待完成") {
            if (!(taskStatus.length == 0)) {
                $(taskStatus).text("待完成");
                $(taskStatus).css({"background": "#fff", "color": "#FF5001", "border": "1px solid #FF5B2F"});
                taskContrail.taskSubmitBtn(taskId);
            }
            if (!(homeTaskStatus.length == 0)) {
                $(homeTaskStatus).text("待完成");
                $(homeTaskStatus).css({"background": "#fff", "color": "#FF5001", "border": "1px solid #FF5B2F"});
                taskContrail.homeSubmitBtn(taskId);
                taskContrail.homeDetailBtn(taskId);
            }
        } else if (state == "可领取") {
            if (!(taskStatus.length == 0)) {
                $(taskStatus).text("可领取");
                $(taskStatus).css({"background": "#FF5B2F", "color": "#fff"});
                taskContrail.taskReceiveBtn(taskId);
            }
            if (!(homeTaskStatus.length == 0)) {
                $(homeTaskStatus).text("可领取");
                $(homeTaskStatus).css({"background": "#FF5B2F", "color": "#fff"});
                taskContrail.homeReceiveBtn(taskId, taskUrl);
                taskContrail.homeDetailBtn(taskId);
            }
        } else {
            if (!(taskStatus.length == 0)) {
                $(taskStatus).text(state);
                $(taskStatus).css({"background": "#cfcfcf", "color": "#fff", "border": "1px solid"});
                // taskContrail.taskSubmitBtn(taskId);
            }
            if (!(homeTaskStatus.length == 0)) {
                $(homeTaskStatus).text(state);
                $(homeTaskStatus).css({"background": "#cfcfcf", "color": "#fff", "border": "1px solid"});
                // taskContrail.homeSubmitBtn(taskId);
                taskContrail.homeDetailBtn(taskId);
            }
        }
    }
}
var goSubmitBtn;
var taskContrail = {
    taskSubmitBtn: function (taskId) {
        $(taskStatus).get(0).addEventListener("touchstart", function () {
            $(this).css({"background": "#cfcfcf"});
        })
        $(taskStatus).get(0).addEventListener("touchend", function () {
            stm_clicki('send', 'event', '我的栗子', '点击', '待完成', 1);
            $(this).css({"background": "#fff"});
            window.location.href = "receive.html?taskId=" + taskId;
        })
    },
    taskReceiveBtn: function (taskId) {
        $(taskStatus).get(0).addEventListener("touchstart", function () {
            $(this).css({"background": "#E35C38"});
        })
        $(taskStatus).get(0).addEventListener("touchend", function () {
            stm_clicki('send', 'event', '我的栗子', '点击', '可领取', 1);
            $(this).css({"background": "#ff5a2f"});
            window.location.href = "receive.html?taskId=" + taskId;
        })
    },
    homeReceiveBtn: function (taskId, taskUrl) {
        $(homeTaskStatus).get(0).addEventListener("touchstart", function () {
            $(this).css({"background": "#E35C38"});
        })
        $(homeTaskStatus).get(0).addEventListener("touchend", function () {
            stm_clicki('send', 'event', '个人中心', '点击', '可领取', 1);
            $(this).css({"background": "#ff5a2f"});
            taskContrail.HprivilegeUserJudge(function () {
                taskContrail.receiveTasksJudge(taskId, function () {
                    $(".receiveSuccess_modal").css({"display": "block"});
                    goSubmitBtn = document.createElement("div");
                    goSubmitBtn.setAttribute("class", "goSubmitBtn goSubmitBtn_" + taskId);
                    $(goSubmitBtn).text("马上做任务");
                    $(".goSubmitBtnBox").empty();
                    $(".goSubmitBtnBox").append(goSubmitBtn);
                    $(goSubmitBtn).get(0).addEventListener("touchstart", function () {
                        $(this).css({"background": "#E35C38"});
                    })
                    $(goSubmitBtn).get(0).addEventListener("touchend", function () {
                        stm_clicki('send', 'event', '个人中心', '点击', '马上做任务', 1);
                        $(this).css({"background": "#ff5a2f"});
                        window.location.href = taskUrl;
                    })
                    $(".receiveSuccess_close").get(0).addEventListener("touchstart", function () {
                        taskController.taskCardsReload();
                        taskController.swiperReload();
                        $(".receiveSuccess_modal").css({"display": "none"});
                        // window.location.reload();
                    })
                })
            });
        })
    },
    homeSubmitBtn: function (taskId) {
        $(homeTaskStatus).get(0).addEventListener("touchstart", function () {
            $(this).css({"background": "#cfcfcf"});
        })
        $(homeTaskStatus).get(0).addEventListener("touchend", function () {
            stm_clicki('send', 'event', '个人中心', '点击', '待完成', 1);
            $(this).css({"background": "#fff"});
            window.location.href = "receive.html?taskId=" + taskId;
        })
    },
    homeDetailBtn: function (taskId) {
        $(homeTaskDetail).get(0).addEventListener("touchstart", function () {
            stm_clicki('send', 'event', '个人中心', '点击', '任务详情', 1);
            window.location.href = "receive.html?taskId=" + taskId;
        })
    },
    receiveTasksJudge: function (taskId, callback) {
        var receiveTasks = dataModel.getReceiveTasks(taskId);
        if (receiveTasks.status == 400) {
            alert(receiveTasks.msg);
        } else if (receiveTasks.status == 500) {
            alert("请求失败,请重试!");
        } else {
            callback();
        }
    },
    HprivilegeUserJudge: function (callback) {
        if (privilegeJudge == false) {
            $(".privilegeTip_modal").css({"display": "block"});
            $(".privilegeTip_modal").get(0).addEventListener("touchstart", function () {
                $(this).css({"display": "none"});
            })
        } else {
            callback();
        }
    }
}