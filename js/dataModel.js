/**
 * Created by jiayi.hu on 1/19/17.
 */
var dataModel = {
    url: {
        //个人中心接口
        _memberInfo: "http://wx.gemii.cc/personalFW/updateMemberInfo",
        inviteQrscene: "http://wx.gemii.cc/inviteSub/getInviteQrscene",
        dataClock: "http://wx.gemii.cc/clock/updateClock",
        memberInfo: "http://wx.gemii.cc/personalFW/getMemberInfo?code=",
        QrsceneImage:"http://wx.gemii.cc/inviteSub/getQrsceneImage?fileName=",
        //微任务接口
        listInterationRecord:"https://mpmt.gemii.cc/mpmt/irecord/listInterationRecord?pageNo=",
        dataPutTask:"https://mpmt.gemii.cc/mpmt/tmrecord/updatePutTask",
        receiveTasksInit:"https://mpmt.gemii.cc/mpmt/tasks/receiveTasksInit?taskId=",
        personTasksInit:"https://mpmt.gemii.cc/mpmt/tasks/personTasksInit?memberId=",
        myIntegraTasksInit:"https://mpmt.gemii.cc/mpmt/tasks/myIntegraTasksInit?memberId=",
        receiveTasks:"https://mpmt.gemii.cc/mpmt/tmrecord/receiveTasks?memberId=",
        putTasksInit:"https://mpmt.gemii.cc/mpmt/tasks/putTasksInit?taskId=",
        listTaskMembers:"https://mpmt.gemii.cc/mpmt/tmrecord/listTaskMembersByMemberId?memberId=",
        uploadImage:"https://mpmt.gemii.cc/mpmt/iupload/uploadImage",
        chestnutExplain:"https://mpmt.gemii.cc/mpmt/dict/getChestnutExplain?memberId="
    },
    getChestnutExplain: function () {
        var unionid = $.cookie("unionid_cookie");
        var url = this.url.chestnutExplain + unionid;
        var data = this.getData(url);
        return data;
    },
    getListTaskMembers:function (pageNo) {
        var unionid = $.cookie("unionid_cookie");
        var url = this.url.listTaskMembers + unionid + "&pageNo=" + pageNo;
        var data = this.getData(url);
        return data;
    },
    getPutTasksInit: function (taskId) {
        var unionid = $.cookie("unionid_cookie");
        var url = this.url.putTasksInit + taskId + "&memberId=" + unionid;
        var data = this.getData(url);
        return data;
    },
    pullImage: function (Data) {
        var url = this.url.uploadImage;
        var data = this.postData(url,Data);
        return data;
    },
    //点击领取按钮请求
    getReceiveTasks: function (taskId) {
        var unionid = $.cookie("unionid_cookie");
        var url = this.url.receiveTasks + unionid + "&taskId=" + taskId;
        var data = this.getData(url);
        return data;
    },
    getMyIntegraTasksInit:function (memberId) {
        var url = this.url.myIntegraTasksInit + memberId;
        var data = this.getData(url);
        return data;
    },
    getPersonTasksInit: function (memberId) {
        var url = this.url.personTasksInit + memberId;
        var data = this.getData(url);
        return data;
    },
    getReceiveTasksInit: function (taskId) {
        var unionid = $.cookie("unionid_cookie");
        var url = this.url.receiveTasksInit + taskId + "&memberId=" + unionid;
        var data = this.getData(url);
        return data;
    },
    getListInterationRecord:function (pageNo) {
        var unionid = $.cookie("unionid_cookie");
        var url = this.url.listInterationRecord + pageNo + "&memberId=" + unionid;
        var data = this.getData(url);
        return data;
    },
    getMemberInfo: function (code) {
        var unionid = $.cookie("unionid_cookie");
        var url = this.url.memberInfo + code + "&unionid=" + unionid;
        var data = this.getData(url);
        return data;
    },
    upMemberInfo: function (Data) {
        var url = this.url._memberInfo;
        var data = this.postData(url, Data);
        return data;
    },
    getInviteQrscene: function () {
        var url = this.url.inviteQrscene + "?unionid=" + unionid;
        var data = this.getData(url);
        return data.data;
    },
    upDataClock: function (integration) {
        var url = this.url.dataClock + "?unionid=" + unionid + "&integration=" + integration;
        var data = this.getData(url);
        return data;
    },
    updatePutTask: function (Data) {
        var url = this.url.dataPutTask;
        var data = this.postData(url,Data);
        return data;
    },
    postData: function (url, Data) {
        var arraydata;
        $.ajax({
            data: Data,
            type: "POST",
            url: url,
            dataType: "json",
            async: false,
            cache: false,
            success: function (json) {
                arraydata = eval(json);
            },
            error: function () {
            }
        });
        return arraydata;
    },
    getData: function (url) {
        var arraydata;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "json",
            async: false,
            cache: false,
            success: function (json) {
                arraydata = eval(json)
            },
            error: function () {
            }
        });
        return arraydata;
    }
}