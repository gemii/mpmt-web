/**
 * Created by jiayi.hu on 4/12/17.
 */
var contrail = {
    pageJudge: function (openId, goodId, process) {
        var self = this;
        var clientHeight = document.documentElement.clientHeight;
        var clientWidth = document.documentElement.clientWidth;
        var iframe = document.querySelector("#questionnaire");
        var skipContainer = document.querySelector(".skipContainer");
        var skipContent = document.querySelector(".skipContent");
        var textIcon = document.querySelector(".textIcon");
        var QrContainer = document.querySelector(".QrContainer");
        var QrContent = document.querySelector(".QrContent");
        var loadContainer = document.querySelector(".loadContainer");
        switch (process){
            case  "0":
                //不是粉丝
                stm_clicki('send', 'event', '栗子兑换', '页面', '未关注', 1);
                $(QrContainer).css({"display": "block"});
                $(QrContent).css({"margin": (clientHeight - 610 - 386) / 2 + "px auto"});
                break;
            case  "1":
                //积分足够
                stm_clicki('send', 'event', '栗子兑换', '页面', '积分足够', 1);
                $(iframe).css({"display": "block"});
                $(iframe).attr("src","http://gemii.paperol.cn/jq/13116902.aspx?sojumpparm=" + openId + "_" + goodId);
                $(iframe).css({"height":clientHeight,"width":clientWidth});
                break;
            case  "2":
                //积分不够
                stm_clicki('send', 'event', '栗子兑换', '页面', '积分不足', 1);
                $(skipContainer).css({"display": "block"});
                $(textIcon).css({"background":"url(images/liziLack.png)no-repeat center center"});
                $(skipContent).css({"margin": (clientHeight - 230 - 585) / 2 + "px auto"});
                self.timepiece("http://t.cn/R6oWMy9");
                break;
            case  "3":
                stm_clicki('send', 'event', '栗子兑换', '页面', '商品不足', 1);
                //商品已兑换完
                // $(skipContainer).css({"display": "block"});
                // $(textIcon).css({"background":"url(../images/goodsLack.png)no-repeat center center"});
                // $(skipContent).css({"margin": (clientHeight - 230 - 570) / 2 + "px auto"});
                // self.timepiece("http://t.cn/R6oWMy9");
                window.location.href="goodsLack.html";
                break;
            case  "4":
                stm_clicki('send', 'event', '栗子兑换', '页面', '不可重复兑换', 1);
                //不可重复兑换
                $(skipContainer).css({"display": "block"});
                $(textIcon).css({"background":"url(images/repeat.png)no-repeat center center"});
                $(skipContent).css({"margin": (clientHeight - 230 - 585) / 2 + "px auto"});
                self.timepiece("http://t.cn/RXL7Yrc");
                break;
        }
    },
    timepiece: function (url) {
        var skipTimer = document.querySelector(".skipTimer");
        var times = 3;
        var timer = null;
        timer = setInterval(function () {
            times--;
            $(skipTimer).text(times);
            if (times <= 0) {
                clearInterval(timer);
                window.location.href = url;
            }
            else {
            }
        }, 1000);
    },
};
var dataModule = {
    url: {
        goodsVerify: "https://mpmt.gemii.cc/mpmt/irecord/goodsVerify?openId=",
        getUserInfo: "http://wx.gemii.cc/wx/base/getUserInfo?code="
    },
    getPageJudge: function (openId) {
        var self = this;
        var arraydata;
        $.ajax({
            type: "GET",
            url: self.url.goodsVerify + openId,
//                dataType: "json",
            cache: false,
            success: function (json) {
                arraydata = eval(json);
                console.log(arraydata);
                if (arraydata.status == 500) {
                    alert("请求出错,请退出重试!");
                    return;
                }
                if(arraydata.status == 400){
                    alert(arraydata.msg);
                    return;
                }
                contrail.pageJudge(openId, arraydata.data.goodId, arraydata.data.process);
            },
            error: function () {
                alert("请求出错,请退出重试!");
            }
        });
    },
    getOpenId: function (code) {
        var self = this;
        var arraydata;
        $.ajax({
            type: "GET",
            url: self.url.getUserInfo + code,
            cache: false,
            success: function (json) {
                arraydata = eval(json)
                if (arraydata.status == 500) {
                    alert("请求出错,请退出重试!");
                    return;
                }
                self.getPageJudge(arraydata.data.openid);
            },
            error: function () {
                alert("请求出错,请退出重试!");
            }
        });
    }
}