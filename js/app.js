var unionid;
var integration_haven, integration_button;
var checkIn_days;
var serializeArray = new Array();
var getMemberInfoArray = new Array();
var mycv = document.getElementById("invite_canvas");
var mycxt = mycv.getContext("2d");
var flag = false;
var privilegeJudge = false;
var contrail = {
    run: function () {
        this.userCheckIn();     //用户打卡
        this.inviteToAccount(); //邀请关注公众号
        this.userPrivilege();   //用户特权
        this.userBasicInfo();   //用户基本信息
        this.historyAndRuleView();     //历史记录查看
    },
    userCheckIn: function () {
        $(".privilege_checkIn").get(0).addEventListener("click", function () {
            $(this).addClass("privilege_checkIn_down");
            var integration = integration_button + integration_haven;
            var UpDataClock = dataModel.upDataClock(integration);
            if (UpDataClock.status == 200) {
                $(".privilege_checkIn").val("今日已签到");
                $(".checkIn_days_number").text(UpDataClock.data.clockCon);
                $(".integration_number").text(UpDataClock.data.integration);
                $(".integrationNext").text("明日可领 " + UpDataClock.data.integralNext + " 栗子");
                $(this).attr("disabled", true);
                controller.addIntegral_judge(integration_button);
            }
            setTimeout(function () {
                $(".privilege_checkIn").removeClass("privilege_checkIn_down");
                $(".privilege_checkIn").addClass("privilege_checkIn_after");
            }, 300);

        })
    },
    inviteToAccount: function () {
        $(".invite_button").get(0).addEventListener("touchstart", function () {
            stm_clicki('send', 'event', '个人中心', '点击', '邀请关注', 1);
            controller.invite_display();
            $(".invite_attentionModal").css({"display": "block"});
            setTimeout(function () {
                if (flag == true) {
                    $(".invite_canvas_wait").css({"display": "none"});
                    $(".invite_canvas_map").attr("src", mycv.toDataURL('image/png'));
                    $(".invite_close").css({"display": "block"});
                } else {
                    $(".invite_attentionModal").css({"display": "none"});
                    alert("图片加载失败!");
                }
            }, 3000);
        })
        $(".invite_close").get(0).addEventListener("touchstart", function () {
            $(".invite_attentionModal").fadeOut(200);
        })
    },
    userPrivilege: function () {
        $(".myWelfareMall").get(0).addEventListener("touchstart", function () {
            stm_clicki('send', 'event', '个人中心', '点击', '福栗社', 1);
            window.location.href = "http://e.cn.miaozhen.com/r/k=2032248&p=74DFU&dx=__IPDX__&rt=2&ns=__IP__&ni=__IESID__&v=__LOC__&nd=__DRA__&np=__POS__&nn=__APP__&vo=39982c59c&vr=2&o=https%3A%2F%2Flemaihuiyuangou.com%2Fleqee%2F";
        })
        $(".privilege_link").get(0).addEventListener("touchstart", function () {
            window.location.href = "http://t.cn/R6arMXj";
        })
        $(".userInfo_list_tag").get(0).addEventListener("touchstart", function () {
            $(".profilePrivilege_modal").fadeIn(300);
        })
        $(".Privilege_close").get(0).addEventListener("touchstart", function () {
            $(".profilePrivilege_modal").fadeOut(300);
        })
        $(".myIntegration").get(0).addEventListener("touchstart", function () {
            stm_clicki('send', 'event', '个人中心', '点击', '我的栗子', 1);
            window.location.href = "receiveTask.html";
        })
    },
    userBasicInfo: function () {
        $(".show_edit").get(0).addEventListener("click", function () {
            stm_clicki('send', 'event', '个人中心', '点击', '编辑个人资料', 1);
            $(".userInfoForm_modal").fadeIn(300);
            controller.dataEcho();
        })
        $(".fill_close").get(0).addEventListener("click", function () {
            $(".userInfoForm_modal").fadeOut(300);
        })
        $("#date").get(0).addEventListener("change", function () {
            $("#fill_content_text>span").empty();
            $("#fill_content_text>span").append($(this).get(0).value);
        })
        $(".fill_submit").get(0).addEventListener("touchstart", function () {
            $(".userInfoForm_modal .fill_submit").css({"background": "#EE5000"});
        })
        $(".fill_submit").get(0).addEventListener("touchend", function () {
            $(".userInfoForm_modal .fill_submit").css({"background": "#ff5b2f"});
            serializeArray.splice(0, serializeArray.length);
            var mather_name = $("#mather_name").get(0).value;
            var mobile = $("#mobile").get(0).value;
            var address = $("#address").get(0).value;
            var babyDate = $("#fill_content_text>span").get(0).innerText;
            serializeArray.push({"name": "username", "value": mather_name},
                {"name": "telephone", "value": mobile},
                {"name": "address", "value": address},
                {"name": "babyBir", "value": babyDate},
                {"name": "unionid", "value": unionid});
            if ($("#mather_name").val() == ""
                || $("#mobile").val() == ""
                || $("#address").val() == ""
                || $("#fill_content_text>span").text() == "请选择日期") {
                alert("请完成信息填写");
                return false;
            }
            else {
                if (!(/^1[34578]\d{9}$/.test($("#mobile").val()))) {
                    alert("手机号格式不正确");
                    return false;
                }
                else {
                    $(".userInfoForm_modal").fadeOut(300);
                    var memberInfo = dataModel.upMemberInfo(serializeArray);
                    if (memberInfo.status == 200) {
                        $(".fill_tip").css({"visibility": "hidden"});
                        integration_haven = memberInfo.data.member.integration;
                        $(".integration_number").text(integration_haven);
                        controller.dataDisplay(memberInfo);
                        controller.privilegeDisplay(memberInfo);
                        controller.addIntegral_judge(memberInfo.data.addIntegral);
                        // alert("提交成功!");
                    } else {
                        alert("提交失败!");
                        return false;
                    }
                }
            }
        })
        // $(".show_expend_up").get(0).addEventListener("touchstart", function () {
        //     var expend_up = $(this).text() == "更多" ? "收起" : "更多";
        //     $(this).text(expend_up);
        //     $(".active_page").slideToggle();
        //     var end = document.getElementById("end");
        //     setTimeout(function () {
        //             $("body")[0].scrollTop = $("body")[0].offsetHeight ;
        //     },300);
        // });
    },
    historyAndRuleView: function () {
        $(".wechat_tasksHistory").get(0).addEventListener("touchstart", function () {
            stm_clicki('send', 'event', '个人中心', '点击', '查看历史任务', 1);
            window.location.href = "historyView.html";
        })
        $(".doubt_lizi").get(0).addEventListener("touchstart", function () {
            stm_clicki('send', 'event', '个人中心', '点击', '栗子规则', 1);
            window.location.href = "rule.html";
        })
    }
};

var controller = {
    //null转为字符串
    null2string: function (value) {
        var data = value == null || undefined ? "" : value;
        return data;
    },
    dataEcho: function () {
        //数据存下用于回显
        getMemberInfoArray.splice(0, getMemberInfoArray.length);
        $(".show_userInfo").find(".td_second").each(function () {
            getMemberInfoArray.push($(this).text());
        })
        $(".fill_content_text").each(function (i) {
            if (i == 0) {
                $(this).find("input[type='text']").get(0).value = getMemberInfoArray[0];
            }
            if (i == 1) {
                $(this).find("input[type='text']").get(0).value = getMemberInfoArray[1];
            }
            if (i == 3) {
                $(this).find("input[type='text']").get(0).value = getMemberInfoArray[3];
            }
            if (i == 2) {
                if (!(getMemberInfoArray[2] == "" || getMemberInfoArray[2] == undefined)) {
                    $("#fill_content_text>span").text(getMemberInfoArray[2]);
                } else {
                    $("#fill_content_text>span").text("请选择日期");
                }
            }

        })
    },
    dataDisplay: function (member_info) {
        // $(".show_userName").find(".td_second").text(controller.null2string(member_info.data.member.username));
        $(".show_userInfo").find(".td_second").each(function (i) {
            if (i == 0) {
                $(this).text(controller.null2string(member_info.data.member.username));
            }
            if (i == 1) {
                $(this).text(member_info.data.member.telephone);
            }
            if (i == 2) {
                $(this).text(member_info.data.member.babyBir);
            }
            if (i == 3) {
                // $(this).text(member_info.data.babyAge);
                $(this).text(member_info.data.member.address);
            }
        })
    },
    privilegeDisplay: function (member_info) {
        switch (member_info.data.member.userLevel) {
            case "普通会员" :
                // $(".privilege_detail").text("无");
                $(".privilege_link").css({"display": "block"});
                $(".integral_img").css({"display": "none"});
                $(".userInfo_list_tag").removeClass("userInfo_tag");
                privilegeJudge = false;
                break;
            case "栗妈闺蜜":
                // $(".privilege_detail").text("福栗社商品多5%折扣(团购除外)、免费派样优先领取权、福利抽奖中奖率高20%");
                $(".privilege_link").css({"display": "none"});
                $(".integral_img").css({"display": "block"});
                $(".userInfo_list_tag").addClass("userInfo_tag");
                $(".userInfo_list_tag").text(member_info.data.member.userLevel);
                privilegeJudge = true;
                break;
        }
    },
    checkIn_judge: function (member_info) {
        switch (member_info.data.member.isClockToday) {
            case "T":
                integration_button = parseInt(member_info.data.member.integralNext);
                checkIn_days = member_info.data.member.clockCon;
                $(".checkIn_days_number").text(checkIn_days);
                $(".integrationNext").text("明日可领 " + integration_button + " 栗子");
                $(".privilege_checkIn").val("今日已签到");
                $(".privilege_checkIn").attr("disabled", true);
                $(".privilege_checkIn").addClass("privilege_checkIn_after");
                break;
            case "F":
                integration_button = parseInt(member_info.data.member.integralNow);
                checkIn_days = member_info.data.member.clockCon;
                $(".checkIn_days_number").text(checkIn_days);
                $(".integrationNext").text("今日可领 " + integration_button + " 栗子");
                $(".privilege_checkIn").val("今日签到");
                $(".privilege_checkIn").attr("disabled", false);
                break;
        }
    },
    addIntegral_judge: function (response) {
        if (!(response == "" || response == null)) {
            //积分变化提示
            $(".integration_tip").text("今日+" + response + "栗子");
            $(".integration_tip").css({"display": "block"});
            $(".integration_tip").animate({"opacity": 1, "margin-top": -220 + "px"}, 1500, function () {
                $(this).animate({"opacity": 0}, 500, function () {
                    $(this).animate({"margin-top": -110 + "px"}, 0, function () {
                        $(this).css({"display": "none"});
                    });
                });
            });
        }
    },
    //圆角矩形
    invite_display: function () {
        var invite_illustration = new Image();
        invite_illustration.src = "images/illustrationIMG.png";
        var invite_logo = new Image();
        invite_logo.src = "images/logoIMG.png";
        var invite_QRcode = new Image();
        invite_QRcode.crossOrigin = "anonymous";
        invite_QRcode.src = dataModel.url.QrsceneImage + dataModel.getInviteQrscene();
        // invite_QRcode.src = "images/logoIMG.png";
        function drawRoundRect(cxt, x, y, width, height, radius) {
            cxt.beginPath();
            cxt.arc(x + radius, y + radius, radius, Math.PI, Math.PI * 3 / 2);
            cxt.lineTo(width - radius + x, y);
            cxt.arc(width - radius + x, radius + y, radius, Math.PI * 3 / 2, Math.PI * 2);
            cxt.lineTo(width + x, height + y - radius);
            cxt.arc(width - radius + x, height - radius + y, radius, 0, Math.PI * 1 / 2);
            cxt.lineTo(radius + x, height + y);
            cxt.arc(radius + x, height - radius + y, radius, Math.PI * 1 / 2, Math.PI);
            cxt.closePath();
        }

        invite_QRcode.addEventListener("load", function () {
            flag = true;
            $("#invite_canvas").attr("width", 525);
            $("#invite_canvas").attr("height", 727);
            //背景色渐变
            var gradient = mycxt.createLinearGradient(0, 0, 0, 525);
            gradient.addColorStop(0, "rgba(255,156,142,1)");
            gradient.addColorStop(1, "rgba(251,185,151,1)");
            drawRoundRect(mycxt, 0, 0, 525, 727, 15);
            mycxt.fillStyle = gradient;
            mycxt.fill();
            mycxt.fillStyle = "rgba(255,255,255,1)";
            mycxt.textAlign = "center";
            mycxt.font = "30px PingFangSC-Medium";
            mycxt.fillText("请长按下方二维码", mycv.width / 2, 70);
            mycxt.font = "25px PingFangSC-Medium";
            mycxt.fillText("发送给朋友", mycv.width / 2, 120);
            mycxt.font = "25px PingFangSC-Medium";
            mycxt.fillText("邀请二维码28天之内有效", mycv.width / 2, 160);
            mycxt.drawImage(this, 0, 0, 430, 430, 110, 310, 310, 310);
            mycxt.globalCompositeOperation = "source-over";
            mycxt.drawImage(invite_illustration, 160, 180);
            mycxt.drawImage(invite_logo, 200, 635);
            // $(".invite_canvas_map").attr("src", mycv.toDataURL('image/png'));
        }, false);
    }
}
